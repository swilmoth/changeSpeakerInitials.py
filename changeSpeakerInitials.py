#!/usr/bin/env python
"""
This script changes the initials on ELAN tiers (such as rf@RU) according to a two column file with the new initials and the partipant's name. 
"""
import sys
import codecs
import re
import os
from argparse import ArgumentParser
try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

sys.stdout=codecs.getwriter('utf8')(sys.stdout)

def main():
	parser = ArgumentParser(description = __doc__)
	parser.add_argument('speakerlist', help = 'speaker list')
	parser.add_argument('elandir', help = 'Directory containing .eaf files')
	opts = parser.parse_args()

	# Making new speaker/initial dictionary
	newSpeakerList = {}
	for line in codecs.open(opts.speakerlist, 'r', 'utf8'):
		line = line.strip('\n')
		name = line.split("\t")[1]
		name = name.lower()
		underscorename = name.replace(' ','_')
		newInitials = line.split("\t")[0]
		newSpeakerList[name] = newInitials
		newSpeakerList[underscorename] = newInitials
	
	# for Testing newSpeakerList
	#for name in newSpeakerList:
	#	print "%s\t%s" %(name, newSpeakerList[name])

	outDir = os.getcwd() + "/UpdatedInitials"
	
	# Using ElementTree to parse/modify all ELAN files in a directory.
	unknownNames = {}
	for root, dirs, files in os.walk(opts.elandir, topdown=False):
		for filename in files:
			fullpath = os.path.join(root,filename)
			if filename.endswith(".eaf"):
				correctedFile = False
				tree = ET.parse(open(fullpath, 'r'))
				elanroot = tree.getroot()

				# Iterating through all tier elements once to make a dictionary.
				elanParticipants = {}
				for element in elanroot.iter('TIER'):
					participant = ""
					tier_id = ""
					old_inits = ""
					if 'PARTICIPANT' in element.attrib:
						participant = element.attrib['PARTICIPANT']
						participantorig = participant
						participant = participant.lower()
						participant = participant.replace('_',' ')
						if 'TIER_ID' in element.attrib:
							tier_id = element.attrib['TIER_ID']
							if "@" in tier_id and bool(re.search(r'@(.+)', tier_id)):
								old_inits = re.search(r'@(.+)', tier_id).group(1)

						if participant and old_inits:
							if participant not in elanParticipants:
								elanParticipants[participant] = old_inits

						if participant not in newSpeakerList:
							if participantorig not in unknownNames:
								unknownNames[participantorig] = [filename]
							else:
								unknownNames[participantorig].append(filename)
					
					# Modifying TIER_ID and PARENT_REF attributes if initials and names match up
					for element in elanroot.iter('TIER'):
						tier_id = ""
						old_tier_id_inits = ""
						new_tier_id_inits = ""
						parent_ref = ""
						old_parent_ref_inits = ""
						new_parent_ref_inits = ""
						if 'TIER_ID' in element.attrib:
							tier_id = element.attrib['TIER_ID']
							if "@" in tier_id and bool(re.search(r'@([A-Z0-9]+)', tier_id)):
								old_tier_id_inits = re.search(r'@([A-Z0-9]+)', tier_id).group(1)
								for name in elanParticipants:
									if name in newSpeakerList:
										if elanParticipants[name] == old_tier_id_inits:
											new_tier_id_inits = newSpeakerList[name]
											element.attrib['TIER_ID'] = element.attrib['TIER_ID'].replace(old_tier_id_inits, new_tier_id_inits)
											correctedFile = True

						if 'PARENT_REF' in element.attrib:
							parent_ref = element.attrib['PARENT_REF']
							if "@" in parent_ref and bool(re.search(r'@[A-Z0-9]+', parent_ref)):
								old_parent_ref_inits = re.search(r'@([A-Z0-9]+)', parent_ref).group(1)
								for name in elanParticipants:
									if name in newSpeakerList:
										if elanParticipants[name] == old_parent_ref_inits:
											new_parent_ref_inits = newSpeakerList[name]
											element.attrib['PARENT_REF'] = element.attrib['PARENT_REF'].replace(old_parent_ref_inits, new_parent_ref_inits)
											correctedFile = True

				if correctedFile:
					if not os.path.exists(outDir):
						os.makedirs(outDir)

					newFile = outDir + "/" + filename.replace(".eaf",".updated.eaf")
					tree.write(newFile, encoding = "utf-8")

	if unknownNames:
		print >> sys.stderr, "The following participants were not found in the speaker list:"
		for name in unknownNames:
			print >> sys.stderr, name + "\t" + ", ".join(set(unknownNames[name]))

if __name__=="__main__":
	main()
